using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARController : MonoBehaviour
{
    private ARFaceManager arFaceManager;
    private ARRaycastManager arRaycastManager;
    [SerializeField] private GameObject prefab;
    private GameObject placedObject;
    private Vector2 touchPosition;

    private void Awake()
    {
        arFaceManager = GetComponent<ARFaceManager>();
        arRaycastManager = GetComponent<ARRaycastManager>();
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            List<ARRaycastHit> hits = new List<ARRaycastHit>();
            if (arRaycastManager.Raycast(touch.position, hits, UnityEngine.XR.ARSubsystems.TrackableType.Face))
            {
                Pose hitPose = hits[0].pose;

                placedObject = Instantiate(prefab, hitPose.position, hitPose.rotation);
            }
        }

        
    }
}

